//  Author:Frankie.Chu
//  Date:20 November, 2012
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  Modified record:
//
/*******************************************************************************/
#include "MotorSensors.h"

void SensorDriver::init()
{
	int i=0;
	for (i=0;i<=DIGITAL_MAX;i++)
	{
		DIO[i].pin=i;
		DIO[i].io_direction=UNKOWN_DIRECT;
		DIO[i].sensor.type=UNKNOWN_SENSOR;
		DIO[i].sensor.position=SENSOR_POSITION_UNKOWN;
		DIO[i].sensor.value=0;
		DIO[i].sensor.pin=i;
	}
}
void SensorDriver::initSensor(uint8_t pin_type,uint8_t io_direction,uint8_t sensor_type,uint8_t sensor_pin,uint8_t position)
{
	if (( pin_type == DIGITAL) && ( sensor_pin <= DIGITAL_MAX))
	{
		DIO[sensor_pin].pin=sensor_pin;
		DIO[sensor_pin].io_direction=io_direction;
		DIO[sensor_pin].sensor.type=sensor_type;
		DIO[sensor_pin].sensor.position=position;
		DIO[sensor_pin].sensor.value=0;
		DIO[sensor_pin].sensor.pin=sensor_pin;
		if (io_direction == OUTPUT_DIRECT)
		{
			pinMode(sensor_pin,OUTPUT);
		}
		else if (io_direction == INPUT_DIRECT)
		{
			pinMode(sensor_pin,INPUT);
		}
	}
}

int32_t  SensorDriver::getSensorValue(uint8_t pin_type,uint8_t sensor_pin)
{
	int32_t RetVal=-1;
	uint8_t readPin;
	if (( pin_type == DIGITAL) && ( sensor_pin <= DIGITAL_MAX))
        {
		readPin=DIO[sensor_pin].pin;
		DIO[sensor_pin].sensor.value=digitalRead(readPin);
		RetVal=DIO[sensor_pin].sensor.value;
	}
	return RetVal;
}

int32_t  SensorDriver::getAllSensorsValue()
{
	int32_t RetVal=0;
	uint8_t readPin;
	int i=0;
	for (i=0;i<=DIGITAL_MAX;i++)
	{
		if (DIO[i].sensor.type != UNKNOWN_SENSOR) 
		{
			DIO[i].sensor.value=digitalRead(DIO[i].sensor.pin);
			RetVal++;
		}
	}
	return RetVal;
}

int32_t  SensorDriver::ObjectInFront()
{
	int32_t RetVal=FALSE;
	if ((DIO[LEFT_WHISKER_DIO].sensor.value == WHISKER_BLOCK) && (DIO[RIGHT_WHISKER_DIO].sensor.value == WHISKER_BLOCK))
	{
		RetVal=TRUE;
	}
	if ((DIO[LEFT_IR_DIO].sensor.value == IR_BLOCK) && (DIO[RIGHT_IR_DIO].sensor.value == IR_BLOCK))
	{
		RetVal=TRUE;
	}
	return RetVal;
}

int32_t  SensorDriver::ObjectOnRight()
{
	int32_t RetVal=FALSE;
	if ((DIO[LEFT_WHISKER_DIO].sensor.value == WHISKER_UNBLOCK) && (DIO[RIGHT_WHISKER_DIO].sensor.value == WHISKER_BLOCK))
	{
		RetVal=TRUE;
	}
	if ((DIO[LEFT_IR_DIO].sensor.value == IR_UNBLOCK) && (DIO[RIGHT_IR_DIO].sensor.value == IR_BLOCK))
	{
		RetVal=TRUE;
	}
	return RetVal;
}

int32_t  SensorDriver::ObjectOnLeft()
{
	int32_t RetVal=FALSE;
	if ((DIO[LEFT_WHISKER_DIO].sensor.value == WHISKER_BLOCK) && (DIO[RIGHT_WHISKER_DIO].sensor.value == WHISKER_UNBLOCK))
	{
		RetVal=TRUE;
	}
	if ((DIO[LEFT_IR_DIO].sensor.value == IR_BLOCK) && (DIO[RIGHT_IR_DIO].sensor.value == IR_UNBLOCK))
	{
		RetVal=TRUE;
	}
	return RetVal;
}

SensorDriver motorsensors;
