#ifndef __MOTORSENSOR_H__
#define __MOTORSENSOR_H__
#include <Arduino.h>
#define TRUE	1
#define FALSE	0
/******Pins definitions*************/
/*
*/
#define DIGITAL_MAX		14
/**************PIN TYPE**********************/
#define DIGITAL			0
#define ANALOG			1
/**************IO DIRECTION**********************/
#define UNKOWN_DIRECT		0
#define INPUT_DIRECT		1
#define OUTPUT_DIRECT		2
/**************Sensor TYPE**********************/
#define UNKNOWN_SENSOR		0
#define WHISKER_SENSOR		1
#define IR_SENSOR		2

/**************PIN NUMBER**********************/
/* Digital */
#define LEFT_WHISKER_DIO	12
#define RIGHT_WHISKER_DIO	13
#define LEFT_IR_DIO		10
#define RIGHT_IR_DIO		11
/* Analog */

/**************PIN Value**********************/
#define	WHISKER_BLOCK		0
#define WHISKER_UNBLOCK		1
#define	IR_BLOCK		0
#define IR_UNBLOCK		1

#define SENSOR_POSITION_UNKOWN  0
#define SENSOR_POSITION_LEFT  	1
#define SENSOR_POSITION_RIGHT 	2

struct SensorStruct
{
	uint8_t type;
	uint8_t position;
	int32_t value;
	uint8_t pin;
};
struct DigitalIOStruct
{
	uint8_t pin;
	uint8_t io_direction;
	SensorStruct sensor;
};
/**Class for Sensor Shield**/
class SensorDriver
{
	DigitalIOStruct DIO[DIGITAL_MAX];
public:
	void init();
	void initSensor(uint8_t pin_type,uint8_t io_direction,uint8_t sensor_type,uint8_t sensor_pin,uint8_t position);
	int32_t getSensorValue(uint8_t pin_type,uint8_t sensor_pin);
	int32_t getAllSensorsValue();
	int32_t ObjectInFront();
	int32_t ObjectOnRight();
	int32_t ObjectOnLeft();
};
extern SensorDriver motorsensors;

#endif
