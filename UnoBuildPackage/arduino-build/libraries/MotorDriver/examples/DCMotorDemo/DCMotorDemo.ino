//  Demo function:The application method to drive the DC motor.
//  Author:Frankie.Chu
//  Date:20 November, 2012

include "MotorDriver.h"

void setup()
{
	/*Configure the motor A to control the wheel at the left side.*/
	/*Configure the motor B to control the wheel at the right side.*/
	motordriver.init(MOTORB,MOTORA,SPEEDPIN_B,SPEEDPIN_A,DIRECTIONPIN_B,DIRECTIONPIN_A);
	motordriver.setSpeed(SPEED_MAX,MOTOR_POSITION_LEFT);
	motordriver.setSpeed(SPEED_MAX,MOTOR_POSITION_RIGHT);
}
 
void loop()
{
	motordriver.goForward();
	delay(2000);
	motordriver.stop();
	delay(1000);
	motordriver.goBackward();
	delay(2000);
	motordriver.stop();
	delay(1000);
	motordriver.goLeft();
	delay(2000);
	motordriver.stop();
	delay(1000);
	motordriver.goRight();
	delay(2000);
	motordriver.stop();
	delay(1000);
	
}
