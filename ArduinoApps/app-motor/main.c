#include "Arduino.h"
//#include "Wire.h"
#include "Servo.h"
int servoPin=9;

Servo myServo;

void setup() {
	//pinMode(13,OUTPUT);
	myServo.attach(servoPin);
}

void loop() {
	myServo.write(0);
	//digitalWrite(13,HIGH);
	delay(1000);
	myServo.write(180);
	//digitalWrite(13,LOW);
	delay(1000);
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}

