#include "Arduino.h"
void setup() {
	pinMode(13,OUTPUT);
}

void loop() {
	digitalWrite(13,HIGH);
	delay(200);
	digitalWrite(13,LOW);
	delay(200);
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}
