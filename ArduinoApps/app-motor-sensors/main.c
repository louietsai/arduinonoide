#include "Arduino.h"
//#include "Wire.h"
#include "MotorSensors.h"


void setup() {
        motorsensors.init();
        motorsensors.initSensor(DIGITAL,INPUT_DIRECT,WHISKER_SENSOR,LEFT_WHISKER_DIO,SENSOR_POSITION_LEFT);
        motorsensors.initSensor(DIGITAL,INPUT_DIRECT,WHISKER_SENSOR,RIGHT_WHISKER_DIO,SENSOR_POSITION_RIGHT);
        motorsensors.initSensor(DIGITAL,INPUT_DIRECT,IR_SENSOR,LEFT_IR_DIO,SENSOR_POSITION_LEFT);
        motorsensors.initSensor(DIGITAL,INPUT_DIRECT,IR_SENSOR,RIGHT_IR_DIO,SENSOR_POSITION_RIGHT);
	Serial.begin(9600);
}

void loop() {
	int sensor_count=0;
	sensor_count=motorsensors.getAllSensorsValue();
	if (motorsensors.ObjectInFront()==TRUE) 
	{
		//Backword, and Turn Left
		Serial.println("Object In Front");
	}
	else if (motorsensors.ObjectOnRight()==TRUE) 
	{
		// Turn Left
		Serial.println("Object On Right");
	}
	else if (motorsensors.ObjectOnLeft()==TRUE) 
	{
		// Turn Right
		Serial.println("Object On Left");
	}
	
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}

