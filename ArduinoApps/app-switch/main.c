#include "Arduino.h"
int InputPin=2;
void setup() {
	pinMode(InputPin,INPUT);
	Serial.begin(9600);
}

void loop() {
	int SwitchState=digitalRead(InputPin);
	if (SwitchState == HIGH){
		Serial.println("The Switch is on!");
	}
	else {
		Serial.println("The Switch is off!");
	}
	delay(500);
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}
