#include "Arduino.h"
//#include "Wire.h"
int EN1=6;
int EN2=5;
int IN1=7;
int IN2=4;
int pwm=250;

void setup() {
	//pinMode(13,OUTPUT);
	int i;
	for (i=4;i<=7;i++)
		pinMode(i,OUTPUT);
}

void loop() {
	analogWrite(EN1,pwm);
	digitalWrite(IN1,HIGH);
	analogWrite(EN2,pwm);
	digitalWrite(IN2,HIGH);
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}

