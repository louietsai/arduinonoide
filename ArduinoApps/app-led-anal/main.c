#include "Arduino.h"
int led=9;
int brightness=0;
int fadeAmount=5;
void setup() {
	pinMode(led,OUTPUT);
}

void loop() {
	analogWrite(led,brightness);
	brightness=brightness+fadeAmount;
	if ((brightness == 0) || (brightness == 255)){
		fadeAmount = - fadeAmount;
	}
	delay(30);
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}
