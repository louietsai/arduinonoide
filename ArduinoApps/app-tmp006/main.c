#include "Arduino.h"
//#include <Wire.h>
#include "Adafruit_TMP006.h"
// Connect VCC to +3V (its a quieter supply than the 5V supply on an Arduino
// Gnd -> Gnd
// SCL connects to the I2C clock pin. On newer boards this is labeled with SCL
// otherwise, on the Uno, this is A5 on the Mega it is 21 and on the Leonardo/Micro digital 3
// SDA connects to the I2C data pin. On newer boards this is labeled with SDA
// otherwise, on the Uno, this is A4 on the Mega it is 20 and on the Leonardo/Micro digital 2

Adafruit_TMP006 tmp006;

void setup() {
  	Serial.begin(9600);
  	Serial.println("Adafruit TMP006 example");

  	// you can also use tmp006.begin(TMP006_CFG_1SAMPLE) or 2SAMPLE/4SAMPLE/8SAMPLE to have
  	// lower precision, higher rate sampling. default is TMP006_CFG_16SAMPLE which takes
  	// 4 seconds per reading (16 samples)
  	if (! tmp006.begin()) {
    		Serial.println("No sensor found");
    		while (1);
	}
	Serial.println("Send s to enter sleep mode, or w to wake up.  Measurements are not updated while asleep!");
}

void loop() {
  // Check for sleep/wake command.
  while (Serial.available() > 0) {
    char c = Serial.read();
    if (c == 'w') {
      Serial.println("Waking up!");
      tmp006.wake();
    }
    else if (c == 's') {
      Serial.println("Going to sleep!");
      tmp006.sleep();
    }
  }

  // Grab temperature measurements and print them.
  float objt = tmp006.readObjTempC();
  //Serial.print("Object Temperature: "); Serial.print(objt); Serial.println("*C");
  float diet = tmp006.readDieTempC();
  Serial.print("Temperature Object: "); Serial.print(objt); Serial.print("*C"); Serial.print(" Die:");Serial.print(diet); Serial.println("*C ");

  delay(100); // 4 seconds per reading for 16 samples per reading

}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}
