#include "Arduino.h"
int PotPin=0;
void setup() {
	Serial.begin(9600);
}

void loop() {
	int PotReading=analogRead(PotPin);
	int PotDisplayVal=map(PotReading, 0, 1023,0,100);
	Serial.print("VR is at");
	Serial.print(PotDisplayVal);
	//Serial.print(PotReading);
	Serial.println("%");
	delay(500);
}

#ifndef GALILEO
int main() {
#else
int main(int argc, char * argv[]) {
#endif
#ifndef GALILEO 
	init();
#else
	init(argc, argv);
#endif
	setup();
	while(1)
		loop();
}
