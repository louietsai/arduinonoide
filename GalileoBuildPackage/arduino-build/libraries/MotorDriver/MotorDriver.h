#ifndef __MOTORDRIVER_H__
#define __MOTORDRIVER_H__
#include <Arduino.h>
/******Pins definitions*************/
/*
MotorA: 
  5: speed - analog:0~255
  4: direction
	h: clockwise
	l: counter clockwise
MotorB: 
  6: speed - analog:0~255
  7: direction
	h: clockwise
	l: counter clockwise

go forward
   A (s:255  d: h)   B (s:255  d: h)
go backward
   A (s:255  d: l)   B (s:255  d: l)
turn right
   A (s:255  d: l)   B (s:255  d: h)
turn left
   A (s:255  d: h)   B (s:255  d: l)
stop
   A (s:0  d: NA)   B (s:0  d: NA)
*/
/* Digital */
#define MOTORSHIELD_IN1	8
#define MOTORSHIELD_IN2	11
#define MOTORSHIELD_IN3	12
#define MOTORSHIELD_IN4	13
#define DIRECTIONPIN_A		4
#define DIRECTIONPIN_B		7
/* Analog */
#define SPEEDPIN_A		5
#define SPEEDPIN_B		6
/**************Motor ID**********************/
#define MOTORA 			0
#define MOTORB 			1

#define MOTOR_POSITION_LEFT  0
#define MOTOR_POSITION_RIGHT 1
#define MOTOR_CLOCKWISE      0
#define MOTOR_COUNTERCLOCKWISE  1
#define MOTOR_ANTICLOCKWISE  1

#define USE_DC_MOTOR		0
#define SPEED_MAX		255
#define SPEED_MIN		0

struct MotorStruct
{
	uint8_t ID;
	uint8_t position;
	int8_t speed;
	uint8_t direction;
	uint8_t speed_pin;
	uint8_t direction_pin;
};
/**Class for Motor Shield**/
class MotorDriver
{
	MotorStruct motorA;
	MotorStruct motorB;
	MotorStruct motorR;
	MotorStruct motorL;
public:
	void init(uint8_t rightmotorID,uint8_t leftmotorID,uint8_t rspin,uint8_t lspin,uint8_t rdpin,uint8_t ldpin);
	void configure(uint8_t position, uint8_t motorID,uint8_t speedpin,uint8_t directionpin);
	void setSpeed(int8_t speed, uint8_t position);
	void setDirection(uint8_t direction, uint8_t position);
	void rotate(uint8_t direction, uint8_t motor_position);
	void rotateWithPosition(uint8_t direction, uint8_t position);
	void goForwardWithPosition(uint8_t position);
	void goBackwardWithPosition(uint8_t position);
	void goForward();
	void goBackward();
	void goLeft();
	void goRight();
	void stop();
	void stop(uint8_t motorID);
};
extern MotorDriver motordriver;

#endif
