//  Author:Frankie.Chu
//  Date:20 November, 2012
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  Modified record:
//
/*******************************************************************************/
#include "MotorDriver.h"

void MotorDriver::init(uint8_t rightmotorID,uint8_t leftmotorID,uint8_t rspin,uint8_t lspin,uint8_t rdpin,uint8_t ldpin)
{
	pinMode(rspin,OUTPUT);
	pinMode(lspin,OUTPUT);
	pinMode(rdpin,OUTPUT);
	pinMode(ldpin,OUTPUT);
	stop();
	/*Configure the motor to control the wheel at the left side.*/
	configure(MOTOR_POSITION_LEFT,leftmotorID,lspin,ldpin);
	/*Configure the motor to control the wheel at the right side.*/
	configure(MOTOR_POSITION_RIGHT,rightmotorID,rspin,rdpin);
	setSpeed(SPEED_MAX,MOTOR_POSITION_LEFT);
	setSpeed(SPEED_MAX,MOTOR_POSITION_RIGHT);
	setDirection(MOTOR_ANTICLOCKWISE,MOTOR_POSITION_LEFT);
	setDirection(MOTOR_CLOCKWISE,MOTOR_POSITION_RIGHT);
}
void MotorDriver::configure(uint8_t position, uint8_t motorID,uint8_t speedpin,uint8_t directionpin)
{
	if(position == MOTOR_POSITION_LEFT){
		motorL.position = position;
		motorL.ID = motorID;
		motorL.speed_pin = speedpin;
		motorL.direction_pin = directionpin;
	}
	else if(position == MOTOR_POSITION_RIGHT){
		motorR.position = position;
		motorR.ID = motorID;
		motorR.speed_pin = speedpin;
		motorR.direction_pin = directionpin;
	}
}

void MotorDriver::setSpeed(int8_t speed, uint8_t position)
{
	if(position == MOTOR_POSITION_LEFT) 
		motorL.speed = speed;
	else if(position == MOTOR_POSITION_RIGHT) 
		motorR.speed = speed;
}
void MotorDriver::setDirection(uint8_t direction, uint8_t position)
{
	if(position == MOTOR_POSITION_LEFT) 
		motorL.direction = direction;
	else if(position == MOTOR_POSITION_RIGHT) 
		motorR.direction = direction;
}
/**********************************************************************/
/*Function: Get the motor rotate                                  	  */
/*Parameter:-uint8_t direction,Clockwise or anticlockwise;            */
/*          -uint8_t motor_position,MOTOR_POSITION_LEFT or			  */
/*			MOTOR_POSITION_RIGHT;                      				  */
/*Return:	void                      							      */
void MotorDriver::rotate(uint8_t direction, uint8_t motor_position)
{
	rotateWithPosition(direction, motor_position);
}

/**********************************************************************/
/*Function: Get the motor rotate                                  	  */
/*Parameter:-uint8_t direction,Clockwise or anticlockwise;            */
/*          -uint8_t motor_position,MOTORA or MOTORB				  */
/*Return:	void                      							      */
void MotorDriver::rotateWithPosition(uint8_t direction, uint8_t position)
{
	uint8_t in1_level;
	if(MOTOR_CLOCKWISE == direction)
	{
		in1_level = HIGH;
	}
	else 
	{
		in1_level = LOW;
	}
	if(position == MOTOR_POSITION_LEFT)
	{ 
		analogWrite(motorL.speed_pin,motorL.speed);
		digitalWrite(motorL.direction_pin,in1_level);
	}	
	else if(position == MOTOR_POSITION_RIGHT) 
	{
		analogWrite(motorR.speed_pin,motorR.speed);
		digitalWrite(motorR.direction_pin,in1_level);
	}
}
void MotorDriver::goForwardWithPosition(uint8_t position)
{
	rotate(MOTOR_CLOCKWISE,position);
}
void MotorDriver::goBackwardWithPosition(uint8_t position)
{
	rotate(MOTOR_COUNTERCLOCKWISE,position);
}
void MotorDriver::goForward()
{
	//rotate(MOTOR_ANTICLOCKWISE,MOTOR_POSITION_LEFT);
	//rotate(MOTOR_CLOCKWISE,MOTOR_POSITION_LEFT);
	//rotate(MOTOR_CLOCKWISE,MOTOR_POSITION_RIGHT);
	goForwardWithPosition(MOTOR_POSITION_LEFT);
	goForwardWithPosition(MOTOR_POSITION_RIGHT);
}
void MotorDriver::goBackward()
{
	//rotate(MOTOR_ANTICLOCKWISE,MOTOR_POSITION_RIGHT);
	//rotate(MOTOR_CLOCKWISE,MOTOR_POSITION_LEFT);
	//rotate(MOTOR_COUNTERCLOCKWISE,MOTOR_POSITION_RIGHT);
	//rotate(MOTOR_COUNTERCLOCKWISE,MOTOR_POSITION_LEFT);
	goBackwardWithPosition(MOTOR_POSITION_LEFT);
	goBackwardWithPosition(MOTOR_POSITION_RIGHT);
}
void MotorDriver::goLeft()
{
	//rotate(MOTOR_CLOCKWISE,MOTOR_POSITION_RIGHT);
	//rotate(MOTOR_CLOCKWISE,MOTOR_POSITION_LEFT);
	//goBackwardWithPosition(MOTOR_POSITION_LEFT);
	//goForwardWithPosition(MOTOR_POSITION_RIGHT);
	goForwardWithPosition(MOTOR_POSITION_LEFT);
	goBackwardWithPosition(MOTOR_POSITION_RIGHT);
}
void MotorDriver::goRight()
{
	//rotate(MOTOR_ANTICLOCKWISE,MOTOR_POSITION_RIGHT);
	//rotate(MOTOR_ANTICLOCKWISE,MOTOR_POSITION_LEFT);
	//goForwardWithPosition(MOTOR_POSITION_LEFT);
	//goBackwardWithPosition(MOTOR_POSITION_RIGHT);
	goBackwardWithPosition(MOTOR_POSITION_LEFT);
	goForwardWithPosition(MOTOR_POSITION_RIGHT);
}

/*************************************************************/
void MotorDriver::stop()
{
	/*Unenble the pin, to stop the motor. */
	digitalWrite(motorL.speed_pin,LOW);
	digitalWrite(motorR.speed_pin,LOW);
}

/*************************************************************/
void MotorDriver::stop(uint8_t position)
{
	if(position == MOTOR_POSITION_LEFT) 
		digitalWrite(motorL.speed_pin,LOW);
	else if(position == MOTOR_POSITION_RIGHT) 
		digitalWrite(motorR.speed_pin,LOW);
}

MotorDriver motordriver;
