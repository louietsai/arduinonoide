# Project Goal
 arudinonoide is a project to compile arudino code without IDE or xCode
 the code can be compiled by avr-gcc or gcc directly.
 You can write a app once for all Uno, Nano, Galileo.
 You app can have main function which support normal console app programming.

# Support
 Uno, Nano, Intel Galileo
 
# Environment Setup
 For Uno,Nano build env
 Under Linux:
 	http://playground.arduino.cc/Learning/Linux
	Ubuntu 12.04 or later
	$sudo apt-get update && sudo apt-get install arduino arduino-core 
 Under Mac OSX
	http://www.obdev.at/products/crosspack/index.html

 For Galileo
 Under Linux:
	please contact me or use your own galileo ext3 rootfs for cross compile
 Under Mac OSX:
	please contact me or use your own galileo ext3 rootfs for cross compile

# Build Instruction
  BUILD PACKAGE:
	please build build package for related boards first
	For Uno, Nano boards
		1. go to UnoBuildPackage/arduino-build
		2. ./BUILD.sh
	For Galileo 
		1. go to GalileoBuildPackage/GalileoBuildEnv
		2. ./Chroot.sh
		3. copy the GalileoBuildPackage/arduino-build into chroot env
		4. go to arudino-build folder inside chroot env
		5. ./BUILD.sh
  BUILD APP:
	For Uno, Nano boards
		1. go to related App folder
		Uno
		2. make board=uno
		Nano
		2. make board=nano
	For Galileo
		1. copy the app folder into chroot env
		2. go to related App folder
		3. make board=galileo

  UPLOAD APP:
	For Uno, Nano boards
		1. go to related App folder
		Uno
		2. make board=uno upload
		Nano
		2. make board=nano upload
        For Galileo
		1. copy the main.elf to the Galileo board directly
		2. ./main.elf to run it

